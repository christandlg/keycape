Project to create a multifunction bluetooth accessory for key organizers, based on the [STM32WB5MMG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb5mmg.html) module.

# Features:
 - USB-C connector with USB-PD support
 - 100 mAh-ish LiPo battery 
 - Micro Power Bank feature
 - BLE 5.3
 - speaker 
 - RGB LED 
 - 2 buttons


Image (WIP):
![Keycape WIP Render](pcb/keycape_bot.png)
![Keycape WIP Render](pcb/keycape_top.png)